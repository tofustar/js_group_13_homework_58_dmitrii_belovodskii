import { Group } from './group.model';
import { EventEmitter } from '@angular/core';

export class GroupsService {
  groupsChange = new EventEmitter<Group[]>();
  private groups: Group[] = [
    new Group('Book club'),
    new Group('Programming courses'),
    new Group('Ski club'),
  ];

  getGroups() {
    return this.groups.slice();
  }

  addGroup(group: Group){
    this.groups.push(group);
    this.groupsChange.emit(this.groups);
  }
}
