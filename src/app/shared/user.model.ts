export class User {
  constructor(
    public name: string,
    public email: string,
    public activity: boolean,
    public role: string
  ) {}
}
