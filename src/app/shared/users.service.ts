import { User } from './user.model';
import { EventEmitter } from '@angular/core';

export class UsersService {
  usersChange = new EventEmitter<User[]>();
  private users: User[] = [
    new User('Дмитрий', 'gltofu@mail.ru', true, 'Admin'),
    new User('Анна', 'anna@mail.ru', false, 'User'),
  ];

  getUsers() {
    return this.users.slice();
  }

  addUser(user:User) {
    this.users.push(user);
    this.usersChange.emit(this.users);
  }

}
