import { Component } from '@angular/core';
import { GroupsService } from '../shared/groups.service';
import { Group } from '../shared/group.model';

@Component({
  selector: 'app-new-group',
  templateUrl: './new-group.component.html',
  styleUrls: ['./new-group.component.css']
})
export class NewGroupComponent {
  groupName = '';

  constructor(public groupsService: GroupsService) {}

  addNewGroup() {
    const newGroup = new Group(this.groupName);
    this.groupsService.addGroup(newGroup);
  }
}
